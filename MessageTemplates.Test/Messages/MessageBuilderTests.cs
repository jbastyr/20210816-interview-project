﻿using MessageTemplates.Messages;
using MessageTemplates.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MessageTemplates.Test.Messages
{
    [TestFixture]
    public class MessageBuilderTests
    {
        [Test, TestCaseSource("MessagesToBuild")]
        public void Build_WithPlaceholdersReplaced_ReturnsProperData(MessageTestCaseData data)
        {
            var builder = new MessageBuilder(data.Template).SentAt(data.SentAt).ReplacePlaceholders();

            if (data.Company != null)
                builder.ForCompany(data.Company);

            if (data.Guest != null)
                builder.ToGuest(data.Guest);

            var actual = builder.Build();

            Assert.That(actual, Is.EqualTo(data.Expected));
        }

        private static IEnumerable<TestCaseData> MessagesToBuild()
        {
            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test template {{GUEST_FIRST_NAME}}",
                Template = new Template
                {
                    Body = "Test template {{GUEST_FIRST_NAME}}"
                }
            }).SetName("Template only");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test Good morning",
                SentAt = new DateTime(2021, 8, 16, 5, 0, 0),
                Template = new Template
                {
                    Body = "Test {{GREETING_TIME_OF_DAY}}"
                }
            }).SetName("Template with Morning greeting");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test Good afternoon",
                SentAt = new DateTime(2021, 8, 16, 14, 0, 0),
                Template = new Template
                {
                    Body = "Test {{GREETING_TIME_OF_DAY}}"
                }
            }).SetName("Template with Afternoon greeting");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test Good evening",
                SentAt = new DateTime(2021, 8, 16, 19, 0, 0),
                Template = new Template
                {
                    Body = "Test {{GREETING_TIME_OF_DAY}}"
                }
            }).SetName("Template with Evening greeting");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test Stanta's Workshop NorthPole {{GUEST_FIRST_NAME}}",
                Template = new Template
                {
                    Body = "Test {{COMPANY_NAME}} {{COMPANY_CITY}} {{GUEST_FIRST_NAME}}"
                },
                Company = new Company
                {
                    Name = "Stanta's Workshop",
                    City = "NorthPole",
                    Timezone = "All"
                }
            }).SetName("Template and Company");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test {{COMPANY_NAME}} Nicholas Saint",
                Template = new Template
                {
                    Body = "Test {{COMPANY_NAME}} {{GUEST_LAST_NAME}} {{GUEST_FIRST_NAME}}"
                },
                Guest = new Guest
                {
                    FirstName = "Saint",
                    LastName = "Nicholas"
                }
            }).SetName("Template and Guest");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test {{COMPANY_NAME}} Nicholas Saint Monday, August 16 at 01:01 AM Room 101",
                Template = new Template
                {
                    Body = "Test {{COMPANY_NAME}} {{GUEST_LAST_NAME}} {{GUEST_FIRST_NAME}} {{RESERVATION_START}} Room {{RESERVATION_ROOM_NUMBER}}"
                },
                Guest = new Guest
                {
                    FirstName = "Saint",
                    LastName = "Nicholas",
                    Reservation = new Reservation
                    {
                        RoomNumber = 101,
                        StartTimestamp = 1629075660,
                        EndTimestamp = 1629162060
                    }
                }
            }).SetName("Template and Guest and Reservation");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test Stanta's Workshop Nicholas Saint {{RESERVATION_START}} Room {{RESERVATION_ROOM_NUMBER}}",
                Template = new Template
                {
                    Body = "Test {{COMPANY_NAME}} {{GUEST_LAST_NAME}} {{GUEST_FIRST_NAME}} {{RESERVATION_START}} Room {{RESERVATION_ROOM_NUMBER}}"
                },
                Company = new Company
                {
                    Name = "Stanta's Workshop",
                    City = "NorthPole",
                    Timezone = "All"
                },
                Guest = new Guest
                {
                    FirstName = "Saint",
                    LastName = "Nicholas"
                }
            }).SetName("Template and Company and Guest");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Test Stanta's Workshop Nicholas Saint Monday, August 16 at 01:01 AM Room 101",
                Template = new Template
                {
                    Body = "Test {{COMPANY_NAME}} {{GUEST_LAST_NAME}} {{GUEST_FIRST_NAME}} {{RESERVATION_START}} Room {{RESERVATION_ROOM_NUMBER}}"
                },
                Company = new Company
                {
                    Name = "Stanta's Workshop",
                    City = "NorthPole",
                    Timezone = "All"
                },
                Guest = new Guest
                {
                    FirstName = "Saint",
                    LastName = "Nicholas",
                    Reservation = new Reservation
                    {
                        RoomNumber = 101,
                        StartTimestamp = 1629075660,
                        EndTimestamp = 1629162060
                    }
                }
            }).SetName("Template and Company and Guest and Reservation");

            yield return new TestCaseData(new MessageTestCaseData
            {
                Expected = "Saint, Nicholas, 101, Monday, August 16 at 01:01 AM, Tuesday, August 17 at 01:01 AM, Stanta's Workshop, NorthPole, Good morning",
                SentAt = new DateTime(2021, 8, 16, 5, 0, 0),
                Template = new Template
                {
                    Body = "{{GUEST_FIRST_NAME}}, {{GUEST_LAST_NAME}}, {{RESERVATION_ROOM_NUMBER}}, {{RESERVATION_START}}, {{RESERVATION_END}}, {{COMPANY_NAME}}, {{COMPANY_CITY}}, {{GREETING_TIME_OF_DAY}}"
                },
                Company = new Company
                {
                    Name = "Stanta's Workshop",
                    City = "NorthPole",
                    Timezone = "All"
                },
                Guest = new Guest
                {
                    FirstName = "Saint",
                    LastName = "Nicholas",
                    Reservation = new Reservation
                    {
                        RoomNumber = 101,
                        StartTimestamp = 1629075660,
                        EndTimestamp = 1629162060
                    }
                }
            }).SetName("All objects, all placeholders");
        }

        public class MessageTestCaseData
        {
            public string Expected { get; set; }
            public DateTime SentAt { get; set; }
            public Template Template { get; set; }
            public Company Company { get; set; }
            public Guest Guest { get; set; }
        }
    }
}
