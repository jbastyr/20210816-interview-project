﻿using MessageTemplates.Abstractions;
using MessageTemplates.Messages;
using MessageTemplates.Models;
using Moq;
using System;

namespace MessageTemplates.Test.Mocks
{
    public static class MessageSetup
    {
        public static void SetupFactory_Simple(Mock<IMessageBuilderFactory> builderFactory)
        {
            builderFactory.Setup(factory => factory.CreateFromTemplate(It.IsAny<Template>()))
                .Returns<Template>(
                    (t) => SetupBuilder_Builds_IgnoringNullParameters(new Mock<MessageBuilder>(t), t)
                );
        }

        public static MessageBuilder SetupBuilder_Builds_IgnoringNullParameters(Mock<MessageBuilder> builder, Template t)
        {
            builder.Setup(b => b.ForCompany(It.IsAny<Company>())).Returns(builder.Object);
            builder.Setup(b => b.ToGuest(It.IsAny<Guest>())).Returns(builder.Object);
            builder.Setup(b => b.ReplacePlaceholders()).Returns(builder.Object);
            builder.Setup(b => b.SentAt(It.IsAny<DateTime>())).Returns(builder.Object);
            builder.Setup(b => b.Build()).Returns("template body");

            return builder.Object;
        }
    }
}
