﻿using MessageTemplates.Abstractions;
using MessageTemplates.Models;
using Moq;
using System.Collections.Generic;

namespace MessageTemplates.Test.Mocks
{
    public static class CompanyData
    {
        public static void AddNoCompaniesToDataProvider(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp => dp.GetCompaniesAsync()).ReturnsAsync(new List<Company>());
            dataProvider.Setup(dp => dp.GetCompanyAsync(It.IsAny<long>())).ReturnsAsync(default(Company));
        }

        public static void AddCompaniesToDataProvider(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp => dp.GetCompaniesAsync())
                .ReturnsAsync(new List<Company>()
                {
                    new Company() { Id = 1, Name = "Test Company", City = "Test City", Timezone = "Test Timezone"},
                    new Company() { Id = 2, Name = "Test Company 2", City = "Test City 2", Timezone = "Test Timezone 2"}
                });

            dataProvider.Setup(dp => dp.GetCompanyAsync(It.IsAny<long>()))
                .ReturnsAsync(new Company() { Id = 1, Name = "Test Company", City = "Test City", Timezone = "Test Timezone" });
        }
    }

    public static class GuestData
    {
        public static void AddNoGuestsToDataProvider(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp => dp.GetGuestsAsync()).ReturnsAsync(new List<Guest>());
            dataProvider.Setup(dp => dp.GetGuestAsync(It.IsAny<long>())).ReturnsAsync(default(Guest));
        }

        public static void AddGuests_WithoutReservations_ToDataProvider(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp => dp.GetGuestsAsync())
                .ReturnsAsync(new List<Guest>()
                {
                    new Guest() { Id = 1, FirstName = "Test First Name", LastName = "Test Last Name" },
                    new Guest() { Id = 2, FirstName = "Test First Name 2", LastName = "Test Last Name 2" },
                });

            dataProvider.Setup(dp => dp.GetGuestAsync(It.IsAny<long>()))
                .ReturnsAsync(new Guest() { Id = 1, FirstName = "Test First Name", LastName = "Test Last Name" });
        }
    }

    public static class TemplateData
    {
        public static void AddNoTemplatesToDataProvider(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp => dp.GetTemplatesAsync()).ReturnsAsync(new List<Template>());
            dataProvider.Setup(dp => dp.GetTemplateAsync(It.IsAny<long>())).ReturnsAsync(default(Template));
            SetupAddTemplate(dataProvider);
        }

        public static void AddTemplatesToDataProvider(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp => dp.GetTemplatesAsync())
                .ReturnsAsync(new List<Template>()
                {
                    new Template() { Id = 1, Title = "Test Title", Body = "Test Body" },
                    new Template() { Id = 2, Title = "Test Title 2", Body = "Test Body 2" }
                });

            dataProvider.Setup(dp => dp.GetTemplateAsync(It.IsAny<long>()))
                .ReturnsAsync(new Template() { Id = 1, Title = "Test Title", Body = "Test Body" });

            SetupAddTemplate(dataProvider);
        }

        public static void SetupAddTemplate(Mock<IDataProvider> dataProvider)
        {
            dataProvider.Setup(dp =>
                dp.AddTemplateAsync(It.IsAny<Template>()).Result)
                    .Returns<Template>(
                        (template) => new Template() { Id = 999, Title = template.Title, Body = template.Body }
                    );
        }
    }
}
