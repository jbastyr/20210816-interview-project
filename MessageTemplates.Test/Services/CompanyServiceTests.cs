﻿using MessageTemplates.Abstractions;
using MessageTemplates.Exceptions;
using MessageTemplates.Models;
using MessageTemplates.Services;
using MessageTemplates.Test.Mocks;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace MessageTemplates.Test.Services
{
    [TestFixture]
    public class CompanyServiceTests
    {
        private ICompanyService _companyService;
        private Mock<IDataProvider> _dataProvider;

        [SetUp]
        public void Setup()
        {
            _dataProvider = new Mock<IDataProvider>();
            _companyService = new CompanyService(_dataProvider.Object);
        }

        [Test]
        public async Task GetCompaniesAsync_WithoutCompanies_ReturnsEmptyCollection()
        {
            CompanyData.AddNoCompaniesToDataProvider(_dataProvider);

            var actual = await _companyService.GetCompaniesAsync();

            var expected = Enumerable.Empty<Company>();

            Assert.That(actual.Count(), Is.EqualTo(expected.Count()));
            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [Test]
        public async Task GetCompaniesAsync_WithCompanies_ReturnsAllCompanies()
        {
            CompanyData.AddCompaniesToDataProvider(_dataProvider);

            var actual = await _companyService.GetCompaniesAsync();

            Assert.That(actual.Count, Is.EqualTo(2));
        }

        [Test]
        public void GetCompanyAsync_WithoutCompanies_ThrowsNotFoundException()
        {
            CompanyData.AddNoCompaniesToDataProvider(_dataProvider);

            int target = 1;

            var exception = Assert.ThrowsAsync<NotFoundException>(() => _companyService.GetCompanyAsync(target));

            Assert.That(exception, Is.Not.Null);
            Assert.That(exception.Message, Is.EqualTo($"company with id {target} not found"));
        }

        [Test]
        public async Task GetCompanyAsync_WithCompanies_ReturnsSingleCompany()
        {
            CompanyData.AddCompaniesToDataProvider(_dataProvider);

            int target = 1;

            var actual = await _companyService.GetCompanyAsync(1);

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Id, Is.EqualTo(target));
        }
    }
}
