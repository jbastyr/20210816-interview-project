﻿using MessageTemplates.Abstractions;
using MessageTemplates.Exceptions;
using MessageTemplates.Models;
using MessageTemplates.Services;
using MessageTemplates.Test.Mocks;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace MessageTemplates.Test.Services
{
    [TestFixture]
    public class GuestServiceTests
    {
        private IGuestService _guestService;
        private Mock<IDataProvider> _dataProvider;

        [SetUp]
        public void Setup()
        {
            _dataProvider = new Mock<IDataProvider>();
            _guestService = new GuestService(_dataProvider.Object);
        }

        [Test]
        public async Task GetGuestsAsync_WithoutGuests_ReturnsEmptyCollection()
        {
            GuestData.AddNoGuestsToDataProvider(_dataProvider);

            var actual = await _guestService.GetGuestsAsync();

            var expected = Enumerable.Empty<Guest>();

            Assert.That(actual.Count(), Is.EqualTo(expected.Count()));
            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [Test]
        public async Task GetGuestsAsync_WithGuests_ReturnsAllGuests()
        {
            GuestData.AddGuests_WithoutReservations_ToDataProvider(_dataProvider);

            var actual = await _guestService.GetGuestsAsync();

            Assert.That(actual.Count, Is.EqualTo(2));
        }

        [Test]
        public void GetGuestAsync_WithoutGuests_ThrowsNotFoundException()
        {
            GuestData.AddNoGuestsToDataProvider(_dataProvider);

            int target = 1;

            var exception = Assert.ThrowsAsync<NotFoundException>(() => _guestService.GetGuestAsync(target));

            Assert.That(exception, Is.Not.Null);
            Assert.That(exception.Message, Is.EqualTo($"guest with id {target} not found"));
        }

        [Test]
        public async Task GetGuestAsync_WithGuests_ReturnsSingleGuest()
        {
            GuestData.AddGuests_WithoutReservations_ToDataProvider(_dataProvider);

            int target = 1;

            var actual = await _guestService.GetGuestAsync(1);

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Id, Is.EqualTo(target));
        }
    }
}
