﻿using MessageTemplates.Abstractions;
using MessageTemplates.Models;
using MessageTemplates.Services;
using MessageTemplates.Test.Mocks;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace MessageTemplates.Test.Services
{
    [TestFixture]
    public class MessageServiceTests
    {
        private IMessageService _messageService;
        private Mock<IMessageBuilderFactory> _builderFactory;

        [SetUp]
        public void Setup()
        {
            _builderFactory = new Mock<IMessageBuilderFactory>();

            _messageService = new MessageService(
                _builderFactory.Object,
                new Mock<ICompanyService>().Object,
                new Mock<IGuestService>().Object,
                new Mock<ITemplateService>().Object
                );
        }

        [Test]
        public async Task CreateMessageAsync_WithParameters_BuildsTemplateString()
        {
            MessageSetup.SetupFactory_Simple(_builderFactory);

            var messageParams = new MessageParameters
                {
                    CompanyId = 1,
                    GuestId = 1,
                    TemplateId = 1
                };

            var actual = await _messageService.CreateMessage(messageParams);

            Assert.That(actual, Is.Not.Null);
        }

    }
}
