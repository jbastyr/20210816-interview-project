﻿using MessageTemplates.Abstractions;
using MessageTemplates.Exceptions;
using MessageTemplates.Models;
using MessageTemplates.Services;
using MessageTemplates.Test.Mocks;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace MessageTemplates.Test.Services
{
    [TestFixture]
    public class TemplateServiceTests
    {
        private ITemplateService _templateService;
        private Mock<IDataProvider> _dataProvider;

        [SetUp]
        public void Setup()
        {
            _dataProvider = new Mock<IDataProvider>();
            _templateService = new TemplateService(_dataProvider.Object);
        }

        [Test]
        public async Task GetTemplatesAsync_WithoutTemplates_ReturnsEmptyCollection()
        {
            TemplateData.AddNoTemplatesToDataProvider(_dataProvider);

            var actual = await _templateService.GetTemplatesAsync();

            var expected = Enumerable.Empty<Template>();

            Assert.That(actual.Count(), Is.EqualTo(expected.Count()));
            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [Test]
        public async Task GetTemplatesAsync_WithTemplates_ReturnsAllTemplates()
        {
            TemplateData.AddTemplatesToDataProvider(_dataProvider);

            var actual = await _templateService.GetTemplatesAsync();

            Assert.That(actual.Count, Is.EqualTo(2));
        }

        [Test]
        public void GetTemplateAsync_WithoutTemplates_ThrowsNotFoundException()
        {
            TemplateData.AddNoTemplatesToDataProvider(_dataProvider);

            int target = 1;

            var exception = Assert.ThrowsAsync<NotFoundException>(() => _templateService.GetTemplateAsync(target));

            Assert.That(exception, Is.Not.Null);
            Assert.That(exception.Message, Is.EqualTo($"template with id {target} not found"));
        }

        [Test]
        public async Task GetTemplateAsync_WithTemplates_ReturnsSingleTemplate()
        {
            TemplateData.AddTemplatesToDataProvider(_dataProvider);

            int target = 1;

            var actual = await _templateService.GetTemplateAsync(1);

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Id, Is.EqualTo(target));
        }

        [Test]
        public async Task AddTemplateAsync_WithProperData_ReturnsThatTemplateWithId()
        {
            TemplateData.AddNoTemplatesToDataProvider(_dataProvider);

            var adding = new Template() { Title = "My New Title", Body = "My New Body" };

            var actual = await _templateService.AddTemplateAsync(adding);

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Id, Is.Not.EqualTo(default(long)));
            Assert.That(actual.Title, Is.EqualTo(adding.Title));
            Assert.That(actual.Body, Is.EqualTo(adding.Body));
        }
    }
}
