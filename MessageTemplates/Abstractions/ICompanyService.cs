﻿using MessageTemplates.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Abstractions
{
    public interface ICompanyService
    {
        public Task<IEnumerable<Company>> GetCompaniesAsync();
        public Task<Company> GetCompanyAsync(long id);
    }
}
