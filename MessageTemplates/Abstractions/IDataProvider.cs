﻿using MessageTemplates.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Abstractions
{
    public interface IDataProvider
    {
        public Task<IEnumerable<Company>> GetCompaniesAsync();
        public Task<Company> GetCompanyAsync(long id);
        public Task<IEnumerable<Guest>> GetGuestsAsync();
        public Task<Guest> GetGuestAsync(long id);
        public Task<IEnumerable<Template>> GetTemplatesAsync();
        public Task<Template> GetTemplateAsync(long id);
        public Task<Template> AddTemplateAsync(Template template);
    }
}
