﻿using MessageTemplates.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Abstractions
{
    public interface IGuestService
    {
        public Task<IEnumerable<Guest>> GetGuestsAsync();
        public Task<Guest> GetGuestAsync(long id);
    }
}
