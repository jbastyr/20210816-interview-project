﻿using MessageTemplates.Messages;
using MessageTemplates.Models;

namespace MessageTemplates.Abstractions
{
    public interface IMessageBuilderFactory
    {
        public MessageBuilder CreateFromTemplate(Template template);
    }
}
