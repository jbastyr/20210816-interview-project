﻿using MessageTemplates.Models;
using System.Threading.Tasks;

namespace MessageTemplates.Abstractions
{
    public interface IMessageService
    {
        public Task<string> CreateMessage(MessageParameters messageParameters);
    }
}
