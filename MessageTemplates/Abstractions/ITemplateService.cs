﻿using MessageTemplates.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Abstractions
{
    public interface ITemplateService
    {
        public Task<IEnumerable<Template>> GetTemplatesAsync();
        public Task<Template> GetTemplateAsync(long id);
        public Task<Template> AddTemplateAsync(Template template);
        public IEnumerable<string> GetPlaceholders();
    }
}
