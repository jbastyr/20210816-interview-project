﻿using AutoMapper;
using MessageTemplates.Abstractions;
using MessageTemplates.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MessageTemplates.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICompanyService _companyService;

        public CompanyController(
            IMapper mapper,
            ICompanyService companyService
        )
        {
            _mapper = mapper;
            _companyService = companyService;
        }

        [HttpGet]
        public async Task<IEnumerable<CompanyResponse>> GetCompanies()
        {
            var companies = await _companyService.GetCompaniesAsync();
            return _mapper.Map<IEnumerable<CompanyResponse>>(companies);
        }
    }
}
