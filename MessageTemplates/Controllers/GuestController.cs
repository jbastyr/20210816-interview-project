﻿using AutoMapper;
using MessageTemplates.Abstractions;
using MessageTemplates.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GuestController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGuestService _guestService;

        public GuestController(
            IMapper mapper,
            IGuestService guestService
        )
        {
            _mapper = mapper;
            _guestService = guestService;
        }

        [HttpGet]
        public async Task<IEnumerable<GuestResponse>> GetGuests()
        {
            var guests = await _guestService.GetGuestsAsync();
            return _mapper.Map<IEnumerable<GuestResponse>>(guests);
        }

    }
}
