﻿using AutoMapper;
using MessageTemplates.Abstractions;
using MessageTemplates.DataTransferObjects;
using MessageTemplates.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MessageTemplates.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController
    {
        private readonly IMapper _mapper;
        private readonly IMessageService _messageService;

        public MessageController(
            IMapper mapper,
            IMessageService messageService
        )
        {
            _mapper = mapper;
            _messageService = messageService;
        }

        [HttpPost]
        public async Task<ActionResult<string>> CreateMessage(CreateMessageRequest request)
        {
            if (request.CompanyId == default || request.GuestId == default || request.TemplateId == default)
            {
                return new BadRequestResult();
            }

            var messageParams = _mapper.Map<MessageParameters>(request);

            var message = await _messageService.CreateMessage(messageParams);

            return new CreatedResult(nameof(CreateMessage), message);
        }
    }
}
