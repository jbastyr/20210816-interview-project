﻿using AutoMapper;
using MessageTemplates.Abstractions;
using MessageTemplates.DataTransferObjects;
using MessageTemplates.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TemplateController
    {
        private readonly IMapper _mapper;
        private readonly ITemplateService _templateService;

        public TemplateController(
            IMapper mapper,
            ITemplateService templateService
        )
        {
            _mapper = mapper;
            _templateService = templateService;
        }

        [HttpGet]
        public async Task<IEnumerable<TemplateResponse>> GetTemplates()
        {
            var templates = await _templateService.GetTemplatesAsync();
            return _mapper.Map<IEnumerable<TemplateResponse>>(templates);
        }

        [HttpGet("placeholders")]
        public IEnumerable<string> GetPlaceholders()
        {
            return _templateService.GetPlaceholders();
        }

        [HttpPost]
        public async Task<ActionResult> AddTemplate(AddTemplateRequest request)
        {
            var template = _mapper.Map<Template>(request);
            var created = await _templateService.AddTemplateAsync(template);

            return new CreatedResult(nameof(GetTemplates), _mapper.Map<TemplateResponse>(created));
        }
    }
}
