﻿namespace MessageTemplates.DataTransferObjects
{
    public class AddTemplateRequest
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
