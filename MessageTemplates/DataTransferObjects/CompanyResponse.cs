﻿namespace MessageTemplates.DataTransferObjects
{
    public class CompanyResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Timezone { get; set; }
    }
}
