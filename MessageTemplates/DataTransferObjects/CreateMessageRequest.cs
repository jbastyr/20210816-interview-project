﻿using System.ComponentModel.DataAnnotations;

namespace MessageTemplates.DataTransferObjects
{
    public class CreateMessageRequest
    {
        [Required]
        public long TemplateId { get; set; }
        [Required]
        public long GuestId { get; set; }
        [Required]
        public long CompanyId { get; set; }
    }
}
