﻿namespace MessageTemplates.DataTransferObjects
{
    public class GuestResponse
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ReservationResponse Reservation { get; set; }
    }
}
