﻿namespace MessageTemplates.DataTransferObjects
{
    public class ReservationResponse
    {
        public short RoomNumber { get; set; }
        public long StartTimestamp { get; set; }
        public long EndTimestamp { get; set; }
    }
}
