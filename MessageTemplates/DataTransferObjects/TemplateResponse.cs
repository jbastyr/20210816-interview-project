﻿namespace MessageTemplates.DataTransferObjects
{
    public class TemplateResponse
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
