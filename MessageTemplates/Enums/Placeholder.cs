﻿namespace MessageTemplates.Enums
{
    public enum Placeholder
    {
        GUEST_FIRST_NAME,
        GUEST_LAST_NAME,
        RESERVATION_ROOM_NUMBER,
        RESERVATION_START,
        RESERVATION_END,
        COMPANY_NAME,
        COMPANY_CITY,
        GREETING_TIME_OF_DAY,
    }
}
