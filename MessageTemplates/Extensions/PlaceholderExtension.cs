﻿using MessageTemplates.Enums;
using System;

namespace MessageTemplates.Extensions
{
    public static class PlaceholderExtension
    {
		public static string AsReadable(this Placeholder placeholder)
		{
			return "{{" + Enum.GetName(typeof(Placeholder), placeholder) + "}}";
		}
		public static Placeholder AsPlaceholder<Placeholder>(this string readable)
		{
			return (Placeholder)Enum.Parse(typeof(Placeholder), readable.Replace("{", "").Replace("}", ""));
		}
	}
}
