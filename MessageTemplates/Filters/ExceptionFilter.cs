﻿using MessageTemplates.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.Json;

namespace MessageTemplates.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            int responseStatus;
            string message;

            if (context.Exception is NotFoundException)
            {
                responseStatus = StatusCodes.Status404NotFound;
                message = context.Exception.Message;
            }
            // chain more
            else
            {
                // swallow other errors
                responseStatus = StatusCodes.Status500InternalServerError;
                message = "Internal server error";
            }

            context.ExceptionHandled = true;

            var error = JsonSerializer.Serialize(message);

            var response = context.HttpContext.Response;
            response.StatusCode = responseStatus;
            response.ContentType = "application/json";

            response.WriteAsync(error);
        }
    }
}
