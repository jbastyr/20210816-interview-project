﻿using MessageTemplates.Mappings;
using Microsoft.Extensions.DependencyInjection;

namespace MessageTemplates
{
    public static class MappingModule
    {
        public static void Configure(IServiceCollection services)
        {
            // register all mapping profiles in the same assembly of stub class
            services.AddAutoMapper(typeof(EmptyMappingProfile).Assembly);
        }
    }
}
