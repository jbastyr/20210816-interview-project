﻿using AutoMapper;
using MessageTemplates.DataTransferObjects;
using MessageTemplates.Models;

namespace MessageTemplates.Mappings
{
    public class CompanyMappingProfile : Profile
    {
        public CompanyMappingProfile()
        {
            CreateMap<Company, CompanyResponse>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Timezone, opt => opt.MapFrom(src => src.Timezone));
        }
    }
}
