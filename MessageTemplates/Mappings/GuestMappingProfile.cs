﻿using AutoMapper;
using MessageTemplates.DataTransferObjects;
using MessageTemplates.Models;

namespace MessageTemplates.Mappings
{
    public class GuestMappingProfile : Profile
    {
        public GuestMappingProfile()
        {
            CreateMap<Guest, GuestResponse>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName));

            CreateMap<Reservation, ReservationResponse>()
                .ForMember(dest => dest.RoomNumber, opt => opt.MapFrom(src => src.RoomNumber))
                .ForMember(dest => dest.StartTimestamp, opt => opt.MapFrom(src => src.StartTimestamp))
                .ForMember(dest => dest.EndTimestamp, opt => opt.MapFrom(src => src.EndTimestamp));
        }
    }
}
