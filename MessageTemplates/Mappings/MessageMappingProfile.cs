﻿using AutoMapper;
using MessageTemplates.DataTransferObjects;
using MessageTemplates.Models;

namespace MessageTemplates.Mappings
{
    public class MessageMappingProfile : Profile
    {
        public MessageMappingProfile()
        {
            CreateMap<CreateMessageRequest, MessageParameters>()
                .ForMember(dest => dest.TemplateId, opt => opt.MapFrom(src => src.TemplateId))
                .ForMember(dest => dest.GuestId, opt => opt.MapFrom(src => src.GuestId))
                .ForMember(dest => dest.CompanyId, opt => opt.MapFrom(src => src.CompanyId));
        }
    }
}
