﻿namespace MessageTemplates.Messages
{
    public abstract class BaseHandler
    {
        public abstract void Handle(object sender, MessageData data);
    }
}
