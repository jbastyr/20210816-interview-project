﻿using MessageTemplates.Models;
using System;

namespace MessageTemplates.Messages
{
    public class MessageBuilder
    {
        public event EventHandler<MessageData> OnRender;

        private MessageData _data;

        public MessageBuilder(Template template)
        {
            _data = new MessageData();
            FromTemplate(template);
        }

        private MessageBuilder FromTemplate(Template template)
        {
            _data.Template = template;
            OnRender += new TemplateHandler().Handle;
            return this;
        }

        public virtual MessageBuilder ForCompany(Company company)
        {
            _data.Company = company;
            return this;
        }

        public virtual MessageBuilder ToGuest(Guest guest)
        {
            _data.Guest = guest;
            return this;
        }

        public virtual MessageBuilder SentAt(DateTime datetime)
        {
            _data.SentAt = datetime;
            return this;
        }

        public virtual MessageBuilder ReplacePlaceholders()
        {
            OnRender += new PlaceholderHandler().Handle;
            return this;
        }

        public virtual string Build()
        {
            OnRender?.Invoke(this, _data);

            return _data.WorkingMessage;
        }
    }
}
