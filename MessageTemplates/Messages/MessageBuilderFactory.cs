﻿using MessageTemplates.Abstractions;
using MessageTemplates.Models;

namespace MessageTemplates.Messages
{
    public class MessageBuilderFactory : IMessageBuilderFactory
    {
        public MessageBuilder CreateFromTemplate(Template template)
        {
            return new MessageBuilder(template);
        }
    }
}
