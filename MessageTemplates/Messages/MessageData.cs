﻿using MessageTemplates.Models;
using System;

namespace MessageTemplates.Messages
{
    public class MessageData
    {
        public Company Company { get; set; }
        public Guest Guest { get; set; }
        public Template Template { get; set; }
        public DateTime SentAt { get; set; }
        public string WorkingMessage { get; set; }
    }
}
