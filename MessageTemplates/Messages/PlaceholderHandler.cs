﻿using MessageTemplates.Enums;
using MessageTemplates.Extensions;
using System;

namespace MessageTemplates.Messages
{
    public class PlaceholderHandler : BaseHandler
    {
        readonly string DATE_FORMAT = "dddd, MMMM dd 'at' HH:mm tt";

        public override void Handle(object sender, MessageData data)
        {
            if (string.IsNullOrEmpty(data?.WorkingMessage))
            {
                throw new ArgumentNullException($"{nameof(PlaceholderHandler)} must have a message to replace placeholders within");
            }

            if (data.Company != null)
            {
                ReplacePlaceholder(data, Placeholder.COMPANY_NAME, data.Company, c => c.Name);
                ReplacePlaceholder(data, Placeholder.COMPANY_CITY, data.Company, c => c.City);
            }

            if (data.Guest != null)
            {
                ReplacePlaceholder(data, Placeholder.GUEST_FIRST_NAME, data.Guest, g => g.FirstName);
                ReplacePlaceholder(data, Placeholder.GUEST_LAST_NAME, data.Guest, g => g.LastName);
                if (data.Guest.Reservation != null)
                {
                    ReplacePlaceholder(data, Placeholder.RESERVATION_ROOM_NUMBER, data.Guest.Reservation, r => r.RoomNumber, n => n.ToString());
                    ReplacePlaceholder(data, Placeholder.RESERVATION_START, data.Guest.Reservation, r => r.StartDateTime, n => n.ToString(DATE_FORMAT));
                    ReplacePlaceholder(data, Placeholder.RESERVATION_END, data.Guest.Reservation, r => r.EndDateTime, n => n.ToString(DATE_FORMAT));
                }
            }

            ReplacePlaceholder(data, Placeholder.GREETING_TIME_OF_DAY, data.SentAt, GetDateGreeting);
        }

        private static void ReplacePlaceholder<TSource, TMember>(MessageData data, Placeholder term, TSource source, Func<TSource, TMember> accessor, Func<TMember, string> transformer = null)
        {
            var token = term.AsReadable();
            TMember memberData = accessor(source);

            string memberValue;

            if (transformer != null)
            {
                memberValue = transformer(memberData);
            }
            else if (memberData is string)
            {
                memberValue = memberData as string;
            }
            else
            {
                throw new InvalidOperationException("accessor must return string implicitly or provide transformer func to handle conversion");
            }

            data.WorkingMessage = data.WorkingMessage.Replace(token, memberValue);
        }

        private static string GetDateGreeting(DateTime date)
        {
            if (date.Hour >= 18)
                return "Good evening";
            if (date.Hour >= 12)
                return "Good afternoon";
            return "Good morning";
        }
    }
}
