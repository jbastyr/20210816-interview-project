﻿using System;

namespace MessageTemplates.Messages
{
    public class TemplateHandler : BaseHandler
    {
        public override void Handle(object sender, MessageData data)
        {
            if (string.IsNullOrEmpty(data?.Template?.Body))
            {
                throw new ArgumentNullException($"{nameof(TemplateHandler)} must have a template to initialize message");
            }

            data.WorkingMessage = data?.Template?.Body;
        }
    }
}
