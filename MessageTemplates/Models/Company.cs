﻿using System.Text.Json.Serialization;

namespace MessageTemplates.Models
{
    public class Company
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("company")]
        public string Name { get; set; }
        [JsonPropertyName("city")]
        public string City { get; set; }
        [JsonPropertyName("timezone")]
        public string Timezone { get; set; }
    }
}
