﻿using System.Text.Json.Serialization;

namespace MessageTemplates.Models
{
    public class Guest
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }
        [JsonPropertyName("reservation")]
        public Reservation Reservation { get; set; }
    }
}
