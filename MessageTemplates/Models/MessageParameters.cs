﻿namespace MessageTemplates.Models
{
    public class MessageParameters
    {
        public long CompanyId { get; set; }
        public long GuestId { get; set; }
        public long TemplateId { get; set; }
    }
}
