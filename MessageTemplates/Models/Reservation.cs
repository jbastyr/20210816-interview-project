﻿using System;
using System.Text.Json.Serialization;

namespace MessageTemplates.Models
{
    public class Reservation
    {
        private long _startTimestamp;
        private long _endTimestamp;
        private DateTime _startDateTime;
        private DateTime _endDateTime;

        [JsonPropertyName("roomNumber")]
        public short RoomNumber { get; set; }
        [JsonPropertyName("startTimestamp")]
        public long StartTimestamp
        {
            get => _startTimestamp;
            set
            {
                _startTimestamp = value;
                _startDateTime = new DateTime(1970, 1, 1).AddSeconds(value);
            }
        }

        [JsonPropertyName("endTimestamp")]
        public long EndTimestamp
        {
            get => _endTimestamp;
            set
            {
                _endTimestamp = value;
                _endDateTime = new DateTime(1970, 1, 1).AddSeconds(value);
            }
        }

        public DateTime StartDateTime { get => _startDateTime; }
        public DateTime EndDateTime { get => _endDateTime; }
    }
}
