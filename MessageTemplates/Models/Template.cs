﻿using System.Text.Json.Serialization;

namespace MessageTemplates.Models
{
    public class Template
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("body")]
        public string Body { get; set; }
    }
}
