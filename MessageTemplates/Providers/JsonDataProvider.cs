﻿using MessageTemplates.Abstractions;
using MessageTemplates.Exceptions;
using MessageTemplates.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace MessageTemplates.Providers
{
    public class JsonDataProvider : IDataProvider
    {
        private static readonly string BASE_PATH = "Data";
        private static readonly string COMPANY_FILE = "Companies.json";
        private static readonly string GUEST_FILE = "Guests.json";
        private static readonly string TEMPLATE_FILE = "Templates.json";

        private List<Company> _companies;
        private List<Guest> _guests;
        private List<Template> _templates;

        public async Task<IEnumerable<Company>> GetCompaniesAsync()
        {
            return await LoadCompanies();
        }

        public async Task<Company> GetCompanyAsync(long id)
        {
            return (await LoadCompanies()).SingleOrDefault(company => company.Id == id);
        }

        public async Task<IEnumerable<Guest>> GetGuestsAsync()
        {
            return await LoadGuests();
        }

        public async Task<Guest> GetGuestAsync(long id)
        {
            return (await LoadGuests()).SingleOrDefault(guest => guest.Id == id);
        }

        public async Task<IEnumerable<Template>> GetTemplatesAsync()
        {
            return await LoadTemplates();
        }

        public async Task<Template> GetTemplateAsync(long id)
        {
            return (await LoadTemplates()).SingleOrDefault(template => template.Id == id);
        }

        public async Task<Template> AddTemplateAsync(Template template)
        {
            var maxId = (await LoadTemplates()).Max(t => t.Id);

            template.Id = maxId + 1;

            _templates.Add(template);

            return template;
        }

        // not ideal, generally coming from database and
        //      only need to be sure we have a db context injected
        private async Task<IEnumerable<Company>> LoadCompanies()
        {
            if (_companies == null)
                _companies = await LoadData<Company>(COMPANY_FILE);
            return _companies;
        }

        private async Task<IEnumerable<Guest>> LoadGuests()
        {
            if (_guests == null)
                _guests = await LoadData<Guest>(GUEST_FILE);
            return _guests;
        }

        private async Task<IEnumerable<Template>> LoadTemplates()
        {
            if (_templates == null)
                _templates = await LoadData<Template>(TEMPLATE_FILE);
            return _templates;
        }

        private static async Task<List<T>> LoadData<T>(string resource)
        {
            string path = Path.Combine(BASE_PATH, resource);

            using var stream = File.OpenRead(path);

            var options = new JsonSerializerOptions()
            {
                AllowTrailingCommas = true,
                PropertyNameCaseInsensitive = true,
            };
            
            if (stream == null)
                throw new InvalidOperationException($"no data available at {path}");

            return await JsonSerializer.DeserializeAsync<List<T>>(stream, options);
        }
    }
}
