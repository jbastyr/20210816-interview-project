﻿using Autofac;
using MessageTemplates.Abstractions;
using MessageTemplates.Messages;
using MessageTemplates.Services;
using MessageTemplates.Providers;

namespace MessageTemplates
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CompanyService>().As<ICompanyService>();
            builder.RegisterType<GuestService>().As<IGuestService>();
            builder.RegisterType<TemplateService>().As<ITemplateService>();
            builder.RegisterType<MessageService>().As<IMessageService>();

            builder.RegisterType<MessageBuilderFactory>().As<IMessageBuilderFactory>();
            
            // singleton for in-memory cache of data
            builder.RegisterType<JsonDataProvider>().As<IDataProvider>().SingleInstance();
        }
    }
}
