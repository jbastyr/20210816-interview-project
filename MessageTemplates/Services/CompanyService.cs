﻿using MessageTemplates.Abstractions;
using MessageTemplates.Exceptions;
using MessageTemplates.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IDataProvider _dataProvider;
        public CompanyService(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public async Task<IEnumerable<Company>> GetCompaniesAsync()
            => await _dataProvider.GetCompaniesAsync();

        public async Task<Company> GetCompanyAsync(long id)
        {
            var company = await _dataProvider.GetCompanyAsync(id);

            if (company == null)
                throw new NotFoundException($"company with id {id} not found");

            return company;
        }
    }
}
