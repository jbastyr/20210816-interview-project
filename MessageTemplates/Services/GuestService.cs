﻿using MessageTemplates.Abstractions;
using MessageTemplates.Exceptions;
using MessageTemplates.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessageTemplates.Services
{
    public class GuestService : IGuestService
    {
        private readonly IDataProvider _dataProvider;
        public GuestService(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public async Task<IEnumerable<Guest>> GetGuestsAsync()
            => await _dataProvider.GetGuestsAsync();

        public async Task<Guest> GetGuestAsync(long id)
        {
            var guest = await _dataProvider.GetGuestAsync(id);

            if (guest == null)
                throw new NotFoundException($"guest with id {id} not found");

            return guest;
        }
    }
}
