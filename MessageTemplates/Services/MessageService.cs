﻿using MessageTemplates.Abstractions;
using MessageTemplates.Models;
using System;
using System.Threading.Tasks;

namespace MessageTemplates.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageBuilderFactory _messageBuilderFactory;
        private readonly ICompanyService _companyService;
        private readonly IGuestService _guestService;
        private readonly ITemplateService _templateService;

        public MessageService(
            IMessageBuilderFactory messageBuilderFactory,
            ICompanyService companyService,
            IGuestService guestService,
            ITemplateService templateService
        )
        {
            _messageBuilderFactory = messageBuilderFactory;
            _companyService = companyService;
            _guestService = guestService;
            _templateService = templateService;
        }

        public async Task<string> CreateMessage(MessageParameters messageParameters)
        {
            var company = await _companyService.GetCompanyAsync(messageParameters.CompanyId);
            var guest = await _guestService.GetGuestAsync(messageParameters.GuestId);
            var template = await _templateService.GetTemplateAsync(messageParameters.TemplateId);

            var builder = _messageBuilderFactory
                            .CreateFromTemplate(template)
                            .ForCompany(company)
                            .ToGuest(guest)
                            .SentAt(DateTime.Now)
                            .ReplacePlaceholders();

            var message = builder.Build();

            return message;
        }
    }
}
