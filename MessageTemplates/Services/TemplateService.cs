﻿using MessageTemplates.Abstractions;
using MessageTemplates.Enums;
using MessageTemplates.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MessageTemplates.Extensions;
using MessageTemplates.Exceptions;

namespace MessageTemplates.Services
{
    public class TemplateService : ITemplateService
    {
        private readonly IDataProvider _dataProvider;
        public TemplateService(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public async Task<IEnumerable<Template>> GetTemplatesAsync()
            => await _dataProvider.GetTemplatesAsync();

        public async Task<Template> GetTemplateAsync(long id)
        {
            var template = await _dataProvider.GetTemplateAsync(id);

            if (template == null)
                throw new NotFoundException($"template with id {id} not found");

            return template;
        }
        
        public async Task<Template> AddTemplateAsync(Template template)
            => await _dataProvider.AddTemplateAsync(template);


        public IEnumerable<string> GetPlaceholders()
            => Enum.GetValues(typeof(Placeholder))
                    .Cast<Placeholder>()
                    .Select(p => p.AsReadable());
    }
}
