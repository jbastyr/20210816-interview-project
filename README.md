# 20210816 Interview Project

## Overview

Development + all testing was performed on Windows 10 x64 in Visual Studio 2019. The project should run with just the dotnet binary but I did not test this

## Design choices
I went with a pretty standard REST API for this project because it was an easy enough way to set things up and keep functionality separate. It is also what I am most familiar with from recent development efforts. Initially I was planning on adding a UI to this as well, but due to lack of time I did not get to it

Architecture follows a pretty standard setup for IOC of Service dependencies that Controllers and other Services consume

Message Builder (where the magic happens) is an amalgamation of a few design patterns including Factory, Builder, and Filter. It's a bit over-engineered but should be setup in a such a way to support extension without modification, satisfying OCP in this sense.

Additional filter delegates can be added to the MessageBuilder's OnRender event to perform more modifications to the message before being sent. For example, functionality could be added to scan for profanity in messages, or add highlights or styles if the application is extended to support HTML rendering for emails as well

## Why .NET Core + C#
I've had the most experience working with this language and in the web domain, so it was  an easy decision for me to make. That certainly doesn't mean it is the best decision, nor the fastest to implement x, y, or z feature. It's a tool to help solve problems at the end of the day

## External Libraries
- Autofac - simplify parts of dependency injection
- AutoMapper - create structured rules that define how to convert types, eg domain models to data transfer objects and vice versa (also good for database entities <-> domain models)
- Moq - easily mock dependencies for testing
- NUnit - testing framework similar to MSTest but I've find it easier to use for simple cases like this

## Testing the application
There is an NUnit test project setup which has a handful of tests for the Service layer, including for the MessageBuilder. Coverage is not perfect, and there are more edge cases to consider in almost all areas, but as this is a time constrained effort I tested what I thought was most important and most impactful to changes

Throughout the development, I also used a REST client (Insomnia here but any work) to test the endpoints and check how it responded for different data. Tests could be added to cover this as well to reduce the amount of manual testing

## Running the API
1. Checkout the repository and open the .sln in Visual Studio or another IDE
0. Optionally restore Nuget packages, though they should be downloaded when the application starts
0. At the top of the IDE, you can change the solution settings to run the project through IIS express or the application in console
    1. If you change the debug profile / properties, you'll need to be aware of the port the application gets hosted on. By default for me, this is :44361 on IIS Express profile
0. Start the application with F5 / Debug > Run / Start green arrow
    1. You may be prompted to trust the self signed SSL certificate, I believe this can be ignored but your browser or REST client will warn you that the certificate is not trusted
0. Have fun creating templates and messages for guests :)

## TODO
Items that I did not have a chance to get to, or would look at implementing if this was more than an interview project:
1. Authentication and Authorization
0. Full REST/CRUD API
0. Store data in DB and add proper Repository Layer with Dapper or Entity Framework
0. Change the general layout of Companies / Guests / Reservations / Templates so they are more normalized
0. Expand test coverage over Controller layer
0. User Interface
0. Multi-tenancy
0. HTML rendering support
0. Scheduling of messages
0. ... etc, There are many ways to expand the application from where it is at now

## API endpoints
If the application is being hosted at https://localhost:44361, the routes here are directly appended eg https://localhost:44361/template

Get all Templates
```http
GET /template
```
<hr>

Create a new Template
```http
POST /template
content-type: application/json
{
    "title": "Title of template",
    "body": "Body of template, can contain placeholders of format: {{PLACEHOLDER}}"
}
```
<hr>

Get a list of all supported Placeholders
```http
GET /template/placeholders
```
<hr>

Get a list of all Companies
```http
GET /company
```
<hr>

Get a list of all Guests
```http
GET /guest
```
<hr>

Create a message from Template, Guest, and Company
```http
POST /message
content-type: application/json
{
    "companyId: 1,
    "guestId": 1,
    "templateId": 1
}
```
